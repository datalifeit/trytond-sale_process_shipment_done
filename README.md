datalife_sale_process_shipment_done
===================================

The sale_process_shipment_done module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_process_shipment_done/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_process_shipment_done)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
